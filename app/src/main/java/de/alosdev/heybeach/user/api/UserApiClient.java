package de.alosdev.heybeach.user.api;

import android.util.JsonReader;
import android.util.JsonWriter;
import android.util.Log;
import de.alosdev.heybeach.BuildConfig;
import de.alosdev.heybeach.common.service.BackgroundHandler;
import de.alosdev.heybeach.common.service.Command;
import de.alosdev.heybeach.user.domain.User;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class UserApiClient {

  private static final String TAG = "UserApiClient";
  private static final String CHARSET_NAME = "UTF-8";

  public interface UserApiClientListener {

    void successfulLogin(String token);
    void successfulRegister(String token);
    void loginFailed();
    void registerFailed();
    void userValidationFailed(boolean isLogin);
    void loadedUser(boolean isLogin, User user);
    void userExistsAlready();

    class NoOp implements UserApiClientListener {

      @Override
      public void successfulLogin(String token) {
        // no-op
      }

      @Override
      public void successfulRegister(String token) {
        // no-op
      }

      @Override
      public void loginFailed() {
        // no-op
      }

      @Override
      public void registerFailed() {
        // no-op
      }

      @Override
      public void userValidationFailed(boolean isLogin) {
        //no-op
      }

      @Override
      public void loadedUser(boolean isLogin, User user) {
        // no-op
      }

      @Override
      public void userExistsAlready() {
        // no-op
      }

    }

  }

  private UserApiClientListener listener = new UserApiClientListener.NoOp();

  private final BackgroundHandler handler;

  public UserApiClient(BackgroundHandler handler) {
    this.handler = handler;
  }

  public void bind(UserApiClientListener listener) {
    this.listener = listener;
  }

  public void unbind() {
    this.listener = new UserApiClientListener.NoOp();
  }

  public void login(String email, String password) {
    handler.executeParallel(new LoginRegisterCommand(true, email, password));
  }

  public void register(final String email, final String password) {
    handler.executeParallel(new LoginRegisterCommand(false, email, password));
  }

  public void logout(User userData) {
    handler.executeParallel(new LogoutCommand(userData.token()));
  }

  public void loadUserData(boolean isLogin, String token) {
    handler.executeParallel(new LoadUserCommand(isLogin, token));
  }

  private HttpURLConnection createHttpUrlConnection(URL url) throws IOException {
    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    connection.setDoInput(true);
    connection.setInstanceFollowRedirects(false);
    connection.setRequestProperty("Cache-Control", "no-cache");
    connection.setUseCaches(false);
    return connection;
  }

  private class LoginRegisterCommand extends Command {

    private static final String KEY_EMAIL = "email";
    private static final String KEY_PASS = "password";
    private static final int DUPLICATE_USER_ERROR_CODE = 11000;

    private final boolean isLogin;
    private final String email;
    private final String password;
    private String token;
    private int errorCode;

    LoginRegisterCommand(boolean isLogin, String email, String password) {
      this.isLogin = isLogin;
      this.email = email;
      this.password = password;
    }

    @Override
    public void doInBackground() {
      try {
        // TODO: 08.03.17 check if server handling camelcase correctly, otherwise just use lowercase
        URL url = new URL(BuildConfig.BASE_API + (isLogin ? "user/login" : "user/register"));
        HttpURLConnection connection = createUrlConnection(url);
        writeOutput(connection);
        int responseCode = connection.getResponseCode();
        if (responseCode == 200) {
          token = connection.getHeaderField("x-auth");
        } else {
          errorCode = readErrorCode(connection.getInputStream());
        }
      } catch (IOException e) {
        Log.e(TAG, "cannot register", e);
      }
    }

    @Override
    public void doInForeground() {
      if (token == null) {
        handleError();
      } else {
        handleSuccess();
      }
    }

    private int readErrorCode(InputStream in) throws IOException {
      JsonReader reader = new JsonReader(new InputStreamReader(in, CHARSET_NAME));
      try {
        reader.beginObject();
        while (reader.hasNext()) {
          String name = reader.nextName();
          if ("code".equals(name)) {
            return reader.nextInt();
          } else {
            reader.skipValue();

          }
        }
        reader.endObject();
      } finally {
        reader.close();
      }

      return -1;
    }

    private void writeOutput(HttpURLConnection connection) throws IOException {
      JsonWriter writer = null;
      try {
        writer = new JsonWriter(new OutputStreamWriter(connection.getOutputStream(), CHARSET_NAME));
        writer.beginObject();
        writer.name(KEY_EMAIL).value(email);
        writer.name(KEY_PASS).value(password);
        writer.endObject();
      } finally {
        if (null != writer) {
          writer.close();
        }
      }
      writer.close();
    }

    private HttpURLConnection createUrlConnection(URL url) throws IOException {
      HttpURLConnection connection = createHttpUrlConnection(url);
      connection.setDoOutput(true);
      connection.setRequestMethod("POST");
      connection.setRequestProperty("Content-Type", "application/json");
      connection.setRequestProperty("charset", "utf-8");
      return connection;
    }

    private void handleError() {
      if (isLogin) {
        listener.loginFailed();
      } else {
        if (errorCode == DUPLICATE_USER_ERROR_CODE) {
          listener.userExistsAlready();
        } else {
          listener.registerFailed();
        }
      }
    }

    private void handleSuccess() {
      if (isLogin) {
        listener.successfulLogin(token);
      } else {
        listener.successfulRegister(token);
      }
    }
  }

  private class LogoutCommand extends Command {

    private final String token;

    LogoutCommand(String token) {
      this.token = token;
    }

    @Override
    public void doInBackground() {
      try {
        URL url = new URL(BuildConfig.BASE_API + "user/logout");
        HttpURLConnection connection = createUrlConnection(url, token);
        int responseCode = connection.getResponseCode();
        Log.d(TAG, "logout got following response code: " + responseCode);
      } catch (IOException e) {
        Log.e(TAG, "cannot logout, but ignore results", e);
      }
    }

    private HttpURLConnection createUrlConnection(URL url, String token) throws IOException {
      HttpURLConnection connection = createHttpUrlConnection(url);
      connection.setRequestMethod("DELETE");
      connection.setRequestProperty("X-Auth", token);
      return connection;
    }
  }

  private class LoadUserCommand extends Command {

    private final boolean isLogin;
    private final String token;
    private User user;

    LoadUserCommand(boolean isLogin, String token) {
      this.isLogin = isLogin;
      this.token = token;
    }

    @Override
    public void doInBackground() {
      try {
        URL url = new URL(BuildConfig.BASE_API + "user/me");
        HttpURLConnection connection = createUrlConnection(url, token);
        int responseCode = connection.getResponseCode();

        if (responseCode == 200) {
          user = readJsonStream(connection.getInputStream());
        }
      } catch (IOException e) {
        Log.e(TAG, "cannot load userdata", e);
      }
    }

    @Override
    public void doInForeground() {
      if (user == null) {
        listener.userValidationFailed(isLogin);
      } else {
        listener.loadedUser(isLogin, user);
      }
    }

    private User readJsonStream(InputStream in) throws IOException {
      User.Builder builder = new User.Builder();
      JsonReader reader = new JsonReader(new InputStreamReader(in, CHARSET_NAME));
      reader.beginObject();
      while (reader.hasNext()) {
        String name = reader.nextName();
        switch (name) {
          case "_id":
            builder.setId(reader.nextString());
            break;
          case "email":
            builder.setName(reader.nextString());
            break;
          default:
            reader.skipValue();
            break;
        }
      }
      reader.endObject();
      reader.close();

      return builder.setToken(token).build();
    }

    private HttpURLConnection createUrlConnection(URL url, String token) throws IOException {
      HttpURLConnection connection = createHttpUrlConnection(url);
      connection.setRequestMethod("GET");
      connection.setRequestProperty("X-Auth", token);
      return connection;

    }
  }
}
