package de.alosdev.heybeach.user;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import de.alosdev.heybeach.HeyBeachApplication;
import de.alosdev.heybeach.R;
import de.alosdev.heybeach.util.EmailMatcher;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class UserFragment extends Fragment implements UserPresenter.UserView {

  private UserPresenter presenter;
  private View progress;
  private View userDetails;
  private View loginPage;
  private Button loginButton;
  private TextView welcomeMessage;
  private TextInputLayout emailLayout;
  private EditText email;
  private TextInputLayout passwordLayout;
  private EditText password;
  private TextInputLayout passwordRepeatLayout;
  private EditText passwordRepeat;

  public static UserFragment newInstance() {
    return new UserFragment();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    HeyBeachApplication.Injector injector = HeyBeachApplication.getInjector(context);
    presenter = new UserPresenter(injector.userRepo(), injector.userApiClient(), new EmailMatcher());
  }

  @Override
  public void onDetach() {
    presenter = null;
    super.onDetach();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.user_fragment, container, false);
    progress = view.findViewById(R.id.progress);
    setupUserDetails(view);
    setupLoginPage(view);
    return view;
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.bind(this);
  }

  @Override
  public void onPause() {
    presenter.unbind();
    super.onPause();
  }

  private void setupUserDetails(View view) {
    userDetails = view.findViewById(R.id.userDetails);
    View logoutButton = view.findViewById(R.id.button_logout);
    welcomeMessage = (TextView) view.findViewById(R.id.welcomeMessage);
    logoutButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        presenter.onLogout();
        email.setText(null);
        password.setText(null);
        passwordRepeat.setText(null);
      }
    });
  }

  private void setupLoginPage(View view) {
    // TODO: 07.03.17 use constraint layout in login page for better arrangement
    loginPage = view.findViewById(R.id.loginPage);
    emailLayout = (TextInputLayout) view.findViewById(R.id.email_layout);
    email = (EditText) view.findViewById(R.id.email);
    passwordLayout = (TextInputLayout) view.findViewById(R.id.password_layout);
    password = (EditText) view.findViewById(R.id.password);
    passwordRepeatLayout = (TextInputLayout) view.findViewById(R.id.password_repeat_layout);
    passwordRepeat = (EditText) view.findViewById(R.id.password_repeat);
    loginButton = (Button) view.findViewById(R.id.button_login);
    View.OnClickListener radioListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (v.getId() == R.id.radio_login) {
          passwordRepeatLayout.setVisibility(GONE);
          loginButton.setText(R.string.user_button_login);
        } else {
          passwordRepeatLayout.setVisibility(VISIBLE);
          loginButton.setText(R.string.user_button_register);
        }
      }
    };
    final RadioButton loginRadio = (RadioButton) view.findViewById(R.id.radio_login);
    loginRadio.setOnClickListener(radioListener);
    view.findViewById(R.id.radio_register).setOnClickListener(radioListener);
    loginButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        presenter.onLogin(loginRadio.isChecked(), email.getText().toString(), password.getText().toString(), passwordRepeat.getText().toString());
      }
    });
  }

  @Override
  public void showUserPage(String id, String name) {
    changeByLoginState(true);
    welcomeMessage.setText(getString(R.string.user_welcome_message, name, id));
  }

  @Override
  public void showLoginPage() {
    changeByLoginState(false);
  }

  private void changeByLoginState(boolean isLoggedIn) {
    progress.setVisibility(GONE);
    userDetails.setVisibility(isLoggedIn ? VISIBLE : GONE);
    loginPage.setVisibility(isLoggedIn ? GONE : VISIBLE);
  }

  @Override
  public void validationErrorEmailEmpty() {
    emailLayout.setError(getString(R.string.user_validation_email_empty));
    email.requestFocus();
  }

  @Override
  public void validEmail() {
    emailLayout.setErrorEnabled(false);
  }

  @Override
  public void validationErrorEmail() {
    emailLayout.setError(getString(R.string.user_validation_email));
    email.requestFocus();
  }

  @Override
  public void validationErrorPassword() {
    passwordLayout.setError(getString(R.string.user_validation_password));
    password.requestFocus();
  }

  @Override
  public void validPassword() {
    passwordLayout.setErrorEnabled(false);
  }

  @Override
  public void validPasswordRepeat() {
    passwordRepeatLayout.setErrorEnabled(false);
  }

  @Override
  public void validationErrorPasswordRepeat() {
    passwordRepeatLayout.setError(getString(R.string.user_validation_password_repeat));
    passwordRepeat.requestFocus();
  }

  @Override
  public void showRegisterFailed() {
    Toast.makeText(getContext(), R.string.user_failed_registration, Toast.LENGTH_LONG).show();
  }

  @Override
  public void showLoginFailed() {
    Toast.makeText(getContext(), R.string.user_failed_login, Toast.LENGTH_LONG).show();
  }

  @Override
  public void showUserExistsAlready() {
    Toast.makeText(getContext(), R.string.user_email_already_exist, Toast.LENGTH_LONG).show();
  }

  @Override
  public void showLoginSuccess() {
    Toast.makeText(getContext(), R.string.user_login_successful, Toast.LENGTH_LONG).show();
  }

  @Override
  public void showRegisterSuccess() {
    Toast.makeText(getContext(), R.string.user_register_successful, Toast.LENGTH_LONG).show();
  }
}
