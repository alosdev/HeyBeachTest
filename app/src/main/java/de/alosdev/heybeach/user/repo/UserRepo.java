package de.alosdev.heybeach.user.repo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import de.alosdev.heybeach.user.domain.User;

public class UserRepo {

  private static final String PREFERENCE_FILE = "UserRepo";
  private static final String TOKEN = "token";
  private static final String NAME = "name";
  private static final String ID = "id";

  private final Context context;

  public UserRepo(Context context) {
    this.context = context;
  }

  public boolean isLoggedIn() {
    return getPreferences().getString(TOKEN, null) != null;
  }

  private SharedPreferences getPreferences() {
    return context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE);
  }

  public void saveUserData(User user) {
    SharedPreferences.Editor editor = getPreferences().edit();
    editor.putString(TOKEN, user.token());
    editor.putString(NAME, user.name());
    editor.putString(ID, user.id());
    editor.apply();
  }

  @SuppressLint("ApplySharedPref")//need it immediately for security reasons
  public void clearUserData() {
    getPreferences().edit().clear().commit();
  }

  public User getUserData() {
    SharedPreferences preferences = getPreferences();
    return new User.Builder()
        .setId(preferences.getString(ID, null))
        .setName(preferences.getString(NAME, null))
        .setToken(preferences.getString(TOKEN, null))
        .build();
  }
}
