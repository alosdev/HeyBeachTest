package de.alosdev.heybeach.user.domain;

import static android.text.TextUtils.isEmpty;

public class User {

  private final String id;
  private final String name;
  private final String token;

  private User(String id, String name, String token) {
    this.id = id;
    this.name = name;
    this.token = token;
  }

  public String id() {
    return id;
  }

  public String name() {
    return name;
  }

  public String token() {
    return token;
  }

  public static class Builder {
    private String id;
    private String name;
    private String token;

    public Builder setId(String id) {
      this.id = id;
      return this;
    }

    public Builder setName(String name) {
      this.name = name;
      return this;
    }

    public Builder setToken(String token) {
      this.token = token;
      return this;
    }

    public User build() {
      if (isEmpty(id) || isEmpty(name) || isEmpty(token)) {
        throw new IllegalArgumentException("fields must be set in advanced");
      }
      return new User(id, name, token);
    }

  }
}
