package de.alosdev.heybeach.user;

import de.alosdev.heybeach.user.api.UserApiClient;
import de.alosdev.heybeach.user.domain.User;
import de.alosdev.heybeach.user.repo.UserRepo;
import de.alosdev.heybeach.util.EmailMatcher;

class UserPresenter implements UserApiClient.UserApiClientListener {

  interface UserView {

    void showUserPage(String id, String name);
    void showLoginPage();
    void validationErrorEmailEmpty();
    void validEmail();
    void validationErrorEmail();
    void validationErrorPassword();
    void validPassword();
    void validPasswordRepeat();
    void validationErrorPasswordRepeat();
    void showRegisterFailed();
    void showLoginFailed();
    void showUserExistsAlready();
    void showLoginSuccess();
    void showRegisterSuccess();

    class NoOp implements UserView {

      @Override
      public void showUserPage(String id, String name) {
        // no-op
      }

      @Override
      public void showLoginPage() {
        // no-op
      }

      @Override
      public void validationErrorEmailEmpty() {
        // no-op
      }

      @Override
      public void validEmail() {
        // no-op
      }

      @Override
      public void validationErrorEmail() {
        // no-op
      }

      @Override
      public void validationErrorPassword() {
        // no-op
      }

      @Override
      public void validPassword() {
        // no-op
      }

      @Override
      public void validPasswordRepeat() {
        // no-op
      }

      @Override
      public void validationErrorPasswordRepeat() {
        // no-op
      }

      @Override
      public void showRegisterFailed() {
        // no-op
      }

      @Override
      public void showLoginFailed() {
        // no-op
      }

      @Override
      public void showUserExistsAlready() {
        // no-op
      }

      @Override
      public void showLoginSuccess() {
        // no-op
      }

      @Override
      public void showRegisterSuccess() {
        // no-op
      }
    }
  }

  private final UserRepo userRepo;
  private final UserApiClient apiClient;
  private final EmailMatcher emailMatcher;
  private UserView view = new UserView.NoOp();

  UserPresenter(UserRepo userRepo, UserApiClient apiClient, EmailMatcher emailMatcher) {
    this.userRepo = userRepo;
    this.apiClient = apiClient;
    this.emailMatcher = emailMatcher;
  }

  void bind(UserView view) {
    this.view = view;
    apiClient.bind(this);
    if (userRepo.isLoggedIn()) {
      User user = userRepo.getUserData();
      view.showUserPage(user.id(), user.name());
    } else {
      view.showLoginPage();
    }
  }

  void onLogin(boolean isLogin, String email, String password, String passwordRepeat) {
    String emailTrimmed = email.trim();
    boolean isValid = validateEmail(emailTrimmed);
    String passwordTrimmed = password.trim();
    isValid &= validatePassword(passwordTrimmed);
    isValid &= validatePasswordRepeat(isLogin, passwordTrimmed, passwordRepeat.trim());

    if (isValid) {
      if (isLogin) {
        apiClient.login(emailTrimmed, passwordTrimmed);
      } else {
        apiClient.register(emailTrimmed, passwordTrimmed);
      }
    }
  }

  private boolean validatePasswordRepeat(boolean isLogin, String trimmedPassword, String passwordRepeat) {
    if (!isLogin) {
      if (trimmedPassword.equals(passwordRepeat.trim())) {
        view.validPasswordRepeat();
        return true;
      } else {
        view.validationErrorPasswordRepeat();
        return false;
      }
    }
    return true;
  }

  private boolean validatePassword(String password) {
    if (password.length() < 6) {
      view.validationErrorPassword();
      return false;
    }
    view.validPassword();
    return true;
  }

  private boolean validateEmail(String email) {
    if (email.length() < 1) {
      view.validationErrorEmailEmpty();
      return false;
    }

    if (emailMatcher.matches(email)) {
      view.validEmail();
      return true;
    }
    view.validationErrorEmail();
    return false;
  }

  @Override
  public void successfulLogin(String token) {
    loadUserData(true, token);
  }

  private void loadUserData(boolean isLogin, String token) {
    apiClient.loadUserData(isLogin, token);
  }

  @Override
  public void successfulRegister(String token) {
    loadUserData(false, token);
  }

  @Override
  public void loginFailed() {
    view.showLoginFailed();
  }

  @Override
  public void registerFailed() {
    view.showRegisterFailed();
  }

  @Override
  public void userValidationFailed(boolean isLogin) {
    if (isLogin) {
      loginFailed();
    } else {
      registerFailed();
    }
  }

  @Override
  public void loadedUser(boolean isLogin, User user) {
    userRepo.saveUserData(user);
    view.showUserPage(user.id(), user.name());
    if (isLogin) {
      view.showLoginSuccess();
    } else {
      view.showRegisterSuccess();
    }
  }

  @Override
  public void userExistsAlready() {
    view.showUserExistsAlready();
  }

  void onLogout() {
    apiClient.logout(userRepo.getUserData());
    userRepo.clearUserData();
    view.showLoginPage();
  }

  void unbind() {
    apiClient.unbind();
    view = new UserView.NoOp();
  }
}
