package de.alosdev.heybeach.util;

import android.util.Patterns;

public class EmailMatcher {

  public boolean matches(String email) {
    return Patterns.EMAIL_ADDRESS.matcher(email).matches();
  }
}
