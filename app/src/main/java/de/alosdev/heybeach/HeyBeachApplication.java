package de.alosdev.heybeach;

import android.app.Application;
import android.content.Context;
import android.net.http.HttpResponseCache;
import android.util.Log;
import de.alosdev.heybeach.common.service.BackgroundHandler;
import de.alosdev.heybeach.common.service.BackgroundHandlerImpl;
import de.alosdev.heybeach.picture.api.PictureApiClient;
import de.alosdev.heybeach.picture.repo.ImageService;
import de.alosdev.heybeach.picture.repo.ImageServiceImpl;
import de.alosdev.heybeach.picture.repo.PictureRepo;
import de.alosdev.heybeach.user.api.UserApiClient;
import de.alosdev.heybeach.user.repo.UserRepo;
import java.io.File;
import java.io.IOException;

public class HeyBeachApplication extends Application {

  private Injector injector;

  @Override
  public void onCreate() {
    super.onCreate();
    // TODO: 09.03.17 if server send correct cahce headers this would work as caching
    try {
      File httpCacheDir = new File(getCacheDir(), "http");
      long httpCacheSize = 20 * 1024 * 1024; // 20 MiB
      HttpResponseCache.install(httpCacheDir, httpCacheSize);
    } catch (IOException e) {
      Log.i("application", "HTTP response cache installation failed:" + e);
    }
    injector = new Injector(this);
  }

  public static Injector getInjector(Context context) {
    return ((HeyBeachApplication) context.getApplicationContext()).injector;
  }

  @SuppressWarnings("WeakerAccess") // it is used outside
  public static class Injector {

    private final Application application;
    private BackgroundHandler handler;
    private ImageService imageService;
    private UserRepo userRepo;
    private PictureRepo pictureRepo;

    public Injector(Application application) {
      this.application = application;
    }

    BackgroundHandler handler() {
      if (null == handler) {
        handler = new BackgroundHandlerImpl();
      }
      return handler;
    }

    public ImageService imageService() {
      if (null == imageService) {
        imageService = new ImageServiceImpl(handler());
      }
      return imageService;
    }

    public UserRepo userRepo() {
      if (null == userRepo) {
        userRepo = new UserRepo(application);
      }
      return userRepo;
    }

    public UserApiClient userApiClient() {
      return new UserApiClient(handler());
    }
    public PictureRepo pictureRepo() {
      if (null == pictureRepo) {
        pictureRepo = new PictureRepo();
      }
      return pictureRepo;
    }

    public PictureApiClient pictureApiClient() {
      return new PictureApiClient(handler());
    }
  }
}
