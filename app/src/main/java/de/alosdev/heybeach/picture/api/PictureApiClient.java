package de.alosdev.heybeach.picture.api;

import android.util.JsonReader;
import android.util.Log;
import de.alosdev.heybeach.BuildConfig;
import de.alosdev.heybeach.common.service.BackgroundHandler;
import de.alosdev.heybeach.common.service.Command;
import de.alosdev.heybeach.picture.domain.Page;
import de.alosdev.heybeach.picture.domain.Picture;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PictureApiClient {

  public interface PictureApiClientListener {

    void pageLoadingFailed();
    void pageLoaded(Page page);

    class NoOp implements PictureApiClientListener {

      @Override
      public void pageLoadingFailed() {
        //no-op
      }

      @Override
      public void pageLoaded(Page page) {
        //no-op
      }
    }

  }

  private final BackgroundHandler handler;

  private PictureApiClientListener listener = new PictureApiClientListener.NoOp();

  public PictureApiClient(BackgroundHandler handler) {
    this.handler = handler;
  }

  public void bind(PictureApiClientListener listener) {
    this.listener = listener;
  }

  public void unbind() {
    listener = new PictureApiClientListener.NoOp();
  }

  public void loadPage(int page) {
    handler.executeParallel(new PageLoaderCommand(page));
  }

  private class PageLoaderCommand extends Command {

    private final int pageNumber;
    private Page page;

    PageLoaderCommand(int pageNumber) {
      this.pageNumber = pageNumber;
    }

    @Override
    public void doInBackground() {
      try {
        URL url = new URL(BuildConfig.BASE_API + "beaches?page=" + pageNumber);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        InputStream in = urlConnection.getInputStream();
        page = readJsonStream(in, pageNumber);
      } catch (IOException e) {
        Log.e("PictureApiClient", "cannot load page", e);
      }
    }

    @Override
    public void doInForeground() {
      if (null == page) {
        listener.pageLoadingFailed();
      } else {
        listener.pageLoaded(page);
      }
    }

    private Page readJsonStream(InputStream in, int page) throws IOException {
      JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
      try {
        return readPage(reader, page);
      } finally {
        reader.close();
      }
    }

    private Page readPage(JsonReader reader, int page) throws IOException {
      List<Picture> pictures = new ArrayList<>(6);

      reader.beginArray();
      while (reader.hasNext()) {
        pictures.add(readPicture(reader));
      }
      reader.endArray();
      return new Page(page, pictures);
    }

    private Picture readPicture(JsonReader reader) throws IOException {
      Picture.Builder builder = new Picture.Builder();

      reader.beginObject();
      while (reader.hasNext()) {
        String name = reader.nextName();
        switch (name) {
          case "_id":
            builder.setId(reader.nextString());
            break;
          case "name":
            builder.setName(reader.nextString());
            break;
          case "url":
            builder.setUrl(reader.nextString());
            break;
          case "width":
            builder.setWidth(reader.nextInt());
            break;
          case "height":
            builder.setHeight(reader.nextInt());
            break;
          default:
            reader.skipValue();
            break;
        }
      }
      reader.endObject();
      return builder.build();
    }
  }
}
