package de.alosdev.heybeach.picture.repo;

import android.graphics.Bitmap;
import de.alosdev.heybeach.common.service.ServiceResultListener;


public interface ImageService {

  void loadImage(ImageServiceCallBack callback);

  void cancelDownload(ImageServiceCallBack clb);

  interface ImageServiceCallBack extends ServiceResultListener<Bitmap> {

    void setUrl(final String url);

    String getUrl();
  }

}
