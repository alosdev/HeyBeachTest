package de.alosdev.heybeach.picture;

import android.util.Log;
import de.alosdev.heybeach.picture.api.PictureApiClient;
import de.alosdev.heybeach.picture.domain.Page;
import de.alosdev.heybeach.picture.domain.Picture;
import de.alosdev.heybeach.picture.repo.PictureRepo;
import java.util.List;

class PicturePresenter implements PictureApiClient.PictureApiClientListener {

  interface PictureView {

    void addPictures(List<Picture> pictures);

    void loadingPageFailed();
    class NoOp implements PictureView {

      @Override
      public void addPictures(List<Picture> pictures) {
        // no-op
      }

      @Override
      public void loadingPageFailed() {
        // no-op
      }

    }
  }

  private final PictureRepo pictureRepo;

  private final PictureApiClient apiClient;

  private PictureView view = new PictureView.NoOp();
  private boolean isLoading;

  PicturePresenter(PictureRepo pictureRepo, PictureApiClient apiClient) {
    this.pictureRepo = pictureRepo;
    this.apiClient = apiClient;
  }

  void bind(PictureView view) {
    this.view = view;
    apiClient.bind(this);
    if (!isLoading) {
      if (pictureRepo.currentPage() == 0) {
        loadNextPage();
      } else {
        view.addPictures(pictureRepo.pictures());
      }
    }
  }

  void unbind() {
    view = new PictureView.NoOp();
    apiClient.unbind();
  }

  void loadNextPageIfNeeded(int totalItemCount, int lastVisibleItemPosition, int columnCount) {
    if (!isLoading && totalItemCount - lastVisibleItemPosition <= columnCount * 2) {
        loadNextPage();
    }
  }

  @Override
  public void pageLoadingFailed() {
    view.loadingPageFailed();
  }

  @Override
  public void pageLoaded(Page page) {
    if (page.pictures().isEmpty()) {
      Log.d("PicturePresenter", "reached last page");
      pictureRepo.reachedLastPage();
      return;
    }
    pictureRepo.addPictures(page);
    isLoading = false;
    view.addPictures(page.pictures());
  }

  private void loadNextPage() {
    if (pictureRepo.currentPage() == PictureRepo.LAST_PAGE) {
      return;
    }
    isLoading = true;
    apiClient.loadPage(pictureRepo.currentPage() + 1);
  }
}
