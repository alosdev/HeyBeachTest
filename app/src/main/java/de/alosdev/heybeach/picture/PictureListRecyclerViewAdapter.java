package de.alosdev.heybeach.picture;

import android.support.percent.PercentLayoutHelper;
import android.support.percent.PercentLayoutHelper.PercentLayoutInfo;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import de.alosdev.heybeach.R;
import de.alosdev.heybeach.picture.domain.Picture;
import de.alosdev.heybeach.picture.widget.LazyLoadingImageView;
import java.util.ArrayList;
import java.util.List;

class PictureListRecyclerViewAdapter extends RecyclerView.Adapter<PictureListRecyclerViewAdapter.PictureViewHolder> {

  private final List<Picture> mValues = new ArrayList<>();
  private final OnListFragmentInteractionListener mListener;

  PictureListRecyclerViewAdapter(List<Picture> items, OnListFragmentInteractionListener listener) {
    mValues.addAll(items);
    mListener = listener;
  }

  @Override
  public PictureViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.picture_item, parent, false);
    return new PictureViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final PictureViewHolder holder, int position) {
    final Picture picture = mValues.get(position);
    holder.picture = picture;
    holder.imageView.setUrl(mValues.get(position).url());
    holder.name.setText(picture.name());

    holder.view.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (null != mListener) {
          mListener.onListFragmentInteraction(holder.imageView, picture);
        }
      }
    });
    ViewGroup.LayoutParams layoutParams = holder.imageView.getLayoutParams();
    if (layoutParams instanceof PercentLayoutHelper.PercentLayoutParams) {
      PercentLayoutInfo percentLayoutInfo = ((PercentLayoutHelper.PercentLayoutParams) layoutParams).getPercentLayoutInfo();
      float ratio = picture.width() * 1f / picture.height() * 1f;
      percentLayoutInfo.aspectRatio = ratio;
      if (holder.itemView.getLayoutParams() instanceof StaggeredGridLayoutManager.LayoutParams && isHighRatio(ratio)) {
        // showing over full span an item if the ratio is really low
        StaggeredGridLayoutManager.LayoutParams staggeredLayoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
        staggeredLayoutParams.setFullSpan(true);
      }
    }
  }

  private boolean isHighRatio(float ratio) {
    return ratio > 1.8f;
  }

  @Override
  public int getItemCount() {
    return mValues.size();
  }

  @Override
  public int getItemViewType(int position) {
    // optimization that it doesn't jump to much around
    final Picture picture = mValues.get(position);
    return (int) (picture.width() * 1f / picture.height()) * 10;
  }

  void addNewPage(List<Picture> pictures) {
    mValues.addAll(pictures);
    notifyDataSetChanged();
  }

  interface OnListFragmentInteractionListener {

    void onListFragmentInteraction(LazyLoadingImageView imageView, Picture picture);
  }

  class PictureViewHolder extends RecyclerView.ViewHolder {

    final View view;
    final LazyLoadingImageView imageView;
    final TextView name;
    Picture picture;

    PictureViewHolder(View view) {
      super(view);
      this.view = view;
      imageView = (LazyLoadingImageView) view.findViewById(R.id.picture);
      name = (TextView) view.findViewById(R.id.name);
    }

    @Override
    public String toString() {
      return super.toString() + " '" + name.getText() + "'";
    }
  }
}
