package de.alosdev.heybeach.picture.detail;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;
import de.alosdev.heybeach.R;
import de.alosdev.heybeach.common.base.BaseActivity;
import de.alosdev.heybeach.picture.widget.LazyLoadingImageView;
import de.alosdev.heybeach.picture.domain.Picture;

public class PictureDetailActivity extends BaseActivity {

  public static final String EXTRA_PICTURE = "picture";

  public static void start(Fragment fragment, View transitionView, Picture picture) {
    Intent intent = new Intent(fragment.getContext(), PictureDetailActivity.class);
    intent.putExtra(EXTRA_PICTURE, picture);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      ActivityOptionsCompat options = ActivityOptionsCompat.
          makeSceneTransitionAnimation(fragment.getActivity(), transitionView, transitionView.getTransitionName());
      fragment.startActivity(intent, options.toBundle());
    } else {
      fragment.startActivity(intent);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.picture_detail_activity);
    Picture picture = getIntent().getParcelableExtra(EXTRA_PICTURE);
    LazyLoadingImageView imageView = (LazyLoadingImageView) findViewById(R.id.picture);
    TextView name = (TextView) findViewById(R.id.name);
    imageView.setUrl(picture.url());
    name.setText(picture.name());
  }
}
