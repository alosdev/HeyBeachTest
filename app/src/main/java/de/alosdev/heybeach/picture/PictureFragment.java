package de.alosdev.heybeach.picture;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import de.alosdev.heybeach.HeyBeachApplication;
import de.alosdev.heybeach.R;
import de.alosdev.heybeach.picture.detail.PictureDetailActivity;
import de.alosdev.heybeach.picture.domain.Picture;
import de.alosdev.heybeach.picture.widget.LazyLoadingImageView;
import java.util.Collections;
import java.util.List;

// TODO: 06.03.17 would be good to have a scaler, so only the correct sized image is downloaded. 
public class PictureFragment extends Fragment implements PictureListRecyclerViewAdapter.OnListFragmentInteractionListener, PicturePresenter.PictureView {

  public static PictureFragment newInstance() {
    return new PictureFragment();
  }

  private PictureListRecyclerViewAdapter adapter;
  private int columnCount;
  private PicturePresenter presenter;
  private RecyclerView.LayoutManager layoutManager;

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    HeyBeachApplication.Injector injector = HeyBeachApplication.getInjector(context);
    presenter = new PicturePresenter(injector.pictureRepo(), injector.pictureApiClient());
  }

  @Override
  public void onDetach() {
    presenter = null;
    super.onDetach();
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    columnCount = getResources().getInteger(R.integer.column_count);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    // TODO: 09.03.17 add loading spinner
    RecyclerView list = (RecyclerView) inflater.inflate(R.layout.picture_fragment, container, false);

    // Set the adapter
    Context context = list.getContext();
    if (columnCount <= 1) {
      list.setLayoutManager(new LinearLayoutManager(context));
    } else {
      list.setLayoutManager(new StaggeredGridLayoutManager(columnCount, StaggeredGridLayoutManager.VERTICAL));
    }
    layoutManager = list.getLayoutManager();
    adapter = new PictureListRecyclerViewAdapter(Collections.<Picture>emptyList(), this);
    list.setAdapter(adapter);
    list.addOnScrollListener(new PageAwareScrollListener());
    return list;
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.bind(this);
  }

  @Override
  public void onPause() {
    presenter.unbind();
    super.onPause();
  }

  @Override
  public void onListFragmentInteraction(LazyLoadingImageView imageView, Picture picture) {
    PictureDetailActivity.start(this, imageView, picture);
  }

  @Override
  public void addPictures(List<Picture> pictures) {
    adapter.addNewPage(pictures);
  }

  @Override
  public void loadingPageFailed() {
    Toast.makeText(getContext(), R.string.picture_loading_failed, Toast.LENGTH_LONG).show();
  }

  private class PageAwareScrollListener extends RecyclerView.OnScrollListener {

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
      super.onScrolled(recyclerView, dx, dy);
      int totalItemCount = layoutManager.getItemCount();
      int lastVisibleItemPosition = 0;
      if (layoutManager instanceof LinearLayoutManager) {
        lastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
      } else if (layoutManager instanceof StaggeredGridLayoutManager) {
        lastVisibleItemPosition = ((StaggeredGridLayoutManager) layoutManager).findLastVisibleItemPositions(null)[columnCount - 1];
      }
      presenter.loadNextPageIfNeeded(totalItemCount, lastVisibleItemPosition, columnCount);
    }
  }
}
