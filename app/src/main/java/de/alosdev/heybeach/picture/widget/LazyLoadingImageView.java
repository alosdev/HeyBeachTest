package de.alosdev.heybeach.picture.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import de.alosdev.heybeach.HeyBeachApplication;
import de.alosdev.heybeach.R;
import de.alosdev.heybeach.picture.repo.ImageService;
import de.alosdev.heybeach.picture.repo.ImageService.ImageServiceCallBack;

import static android.widget.ImageView.ScaleType.FIT_XY;

public class LazyLoadingImageView extends AppCompatImageView implements ImageServiceCallBack {

  private static final int PLACEHOLDER = R.mipmap.ic_launcher_round;

  private ImageService imageService;

  private String url;

  private ScaleType loadedScaleType = ScaleType.FIT_CENTER;

  public LazyLoadingImageView(Context context) {
    super(context);
    initAndInject(context);
  }

  public LazyLoadingImageView(Context context, AttributeSet attrs) {
    super(context, attrs);
    initAndInject(context);
  }

  public LazyLoadingImageView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    initAndInject(context);
  }

  private void initAndInject(Context context) {
    if (isInEditMode()) {
      super.setImageResource(PLACEHOLDER);
    } else {
      imageService = HeyBeachApplication.getInjector(context).imageService();
      setLoadedScaleType(getScaleType());
      setScaleType(loadedScaleType);
    }
  }

  public void setLoadedScaleType(ScaleType loadedScaleType) {
    this.loadedScaleType = loadedScaleType;
  }

  @Override
  public void setImageResource(int resId) {
    setState(State.DONE);
    super.setImageResource(resId);
  }

  @Override
  public void setImageDrawable(Drawable drawable) {
    if (isInEditMode()) {
      super.setImageDrawable(drawable);
    } else {
      setState(State.DONE);
      crossfade(drawable, 400);
    }
  }

  private void crossfade(final Drawable drawable, final int duration) {
    performAnimation(drawable, duration);
  }

  private void performAnimation(final Drawable drawable, final int duration) {
    performOutAnimation(drawable, duration);
  }

  private void performOutAnimation(final Drawable drawable, final int duration) {
    animate().alpha(0.25f).setDuration(duration / 2).setListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationEnd(Animator animation) {
        super.onAnimationEnd(animation);
        performInAnimation(drawable, duration);
      }

      @Override
      public void onAnimationCancel(Animator animation) {
        super.onAnimationCancel(animation);
        performInAnimation(drawable, duration);
      }
    });
  }

  private void performInAnimation(final Drawable drawable, final int duration) {
    super.setImageDrawable(drawable);
    animate().alpha(1f).setDuration(duration / 2).setListener(null);
  }

  @Override
  public void setImageBitmap(Bitmap bm) {
    setState(State.DONE);
    super.setImageBitmap(bm);
  }

  @Override
  public void setImageURI(Uri uri) {
    setState(State.DONE);
    super.setImageURI(uri);
  }

  @Override
  public void success(Bitmap bitmap) {
    setScaleType(loadedScaleType);
    setImageBitmap(bitmap);
  }

  @Override
  public void failed(String message) {
    setState(State.FAILED);
  }

  @Override
  public String getUrl() {
    return url;
  }

  @Override
  public void setUrl(String url) {
    this.url = url;
    if (null == url) {
      reset();
    } else {
      setState(State.LOADING);
    }
  }

  /**
   * Resets the {@link LazyLoadingImageView}. Displays the default image and stops loading.
   */
  public void reset() {
    this.url = null;
    setState(State.RESET);
  }

  protected void setState(State state) {
    switch (state) {
      case LOADING: {
        super.setImageResource(PLACEHOLDER);
        setScaleType(FIT_XY);
        imageService.loadImage(this);
        break;
      }

      case DONE: {
        setScaleType(loadedScaleType);
        imageService.cancelDownload(this);
        break;
      }

      case FAILED: {
        resetImageResource();
        break;
      }

      case RESET: {
        imageService.cancelDownload(this);
        resetImageResource();
        animate().cancel();
        break;
      }

      default: {
        setState(State.FAILED);
        break;
      }
    }
  }

  private void resetImageResource() {
    setImageResource(PLACEHOLDER);
    setScaleType(FIT_XY);
  }

  private enum State {
    LOADING,
    DONE,
    FAILED,
    RESET
  }
}
