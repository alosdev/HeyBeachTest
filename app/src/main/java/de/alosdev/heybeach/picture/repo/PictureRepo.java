package de.alosdev.heybeach.picture.repo;

import de.alosdev.heybeach.picture.domain.Page;
import de.alosdev.heybeach.picture.domain.Picture;
import java.util.ArrayList;
import java.util.List;

public class PictureRepo {

  public static final int LAST_PAGE = -1;
  private List<Picture> pictures = new ArrayList<>(12);
  private int currentPage = 0;

  public List<Picture> pictures() {
    return pictures;
  }

  public int currentPage() {
    return currentPage;
  }

  public void addPictures(Page page) {
    pictures.addAll(page.pictures());
    currentPage = page.pageNumber();
  }

  public void reachedLastPage() {
    currentPage = LAST_PAGE;
  }
}
