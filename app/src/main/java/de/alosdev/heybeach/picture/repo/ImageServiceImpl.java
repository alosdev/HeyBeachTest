package de.alosdev.heybeach.picture.repo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.util.LruCache;
import android.util.Log;
import de.alosdev.heybeach.BuildConfig;
import de.alosdev.heybeach.common.service.BackgroundHandler;
import de.alosdev.heybeach.common.service.Command;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;

public class ImageServiceImpl implements ImageService {

  private static final String TAG = ImageServiceImpl.class.getSimpleName();

  private ConcurrentHashMap<ImageServiceCallBack, Command> callbacks;

  private LruCache<String, Bitmap> imageCache;

  private final BackgroundHandler backgroundHandler;

  public ImageServiceImpl(final BackgroundHandler backgroundHandler) {
    this.backgroundHandler = backgroundHandler;
    callbacks = new ConcurrentHashMap<>();
    // Get max available VM memory, exceeding this amount will throw an
    // OutOfMemory exception. Stored in kilobytes as LruCache takes an
    // int in its constructor.
    final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

    // Use 1/8th of the available memory for this memory cache.
    final int cacheSize = maxMemory / 8;
    imageCache = new LruCache<String, Bitmap>(cacheSize) {
      @Override
      protected int sizeOf(String key, Bitmap bitmap) {
        // The cache size will be measured in kilobytes rather than
        // number of items.
        return bitmap.getByteCount() / 1024;
      }
    };
  }

  @Override
  public void loadImage(ImageServiceCallBack clb) {
    String url = clb.getUrl();
    if (url == null) {
      clb.failed("url is null");
      return;
    }
    if (imageCache == null) {
      clb.failed("image cache is null");
      return;
    }

    Bitmap image = getImageFromCache(url);
    if (null != image) {
      clb.success(image);
    } else {
      ImageLoadCommand imageLoadCommand = new ImageLoadCommand(clb);
      callbacks.put(clb, imageLoadCommand);
      backgroundHandler.executeParallel(imageLoadCommand);
    }
  }

  private Bitmap getImageFromCache(String url) {
    if (null == url) {
      return null;
    }

    Bitmap image = imageCache.get(url);

    if ((image != null) && !image.isRecycled()) {
      return image;
    }
    return null;
  }

  private class ImageLoadCommand extends Command {

    private final ImageServiceCallBack clb;

    private boolean isSuccess = false;

    private Bitmap image;

    private String url;

    ImageLoadCommand(ImageServiceCallBack clb) {
      this.clb = clb;
    }

    @Override
    public void doInBackground() {
      url = clb.getUrl();
      image = loadImageSynchronous(url);
      if (null != image) {
        isSuccess = true;
      }
    }

    @Override
    public void doInForeground() {
      if (isSuccess) {
        for (ImageServiceCallBack callback : callbacks.keySet()) {
          if (url.equals(callback.getUrl())) {
            callback.success(image);
            removeCallback(callback);
          }
        }
      } else {
        clb.failed("cannot load image");
        removeCallback(clb);
        for (ImageServiceCallBack clbTmp : callbacks.keySet()) {
          if ((null != clb.getUrl()) && clb.getUrl().equals(clbTmp.getUrl())) {
            clbTmp.failed("cannot load image");
            removeCallback(clbTmp);
          }
        }
      }
    }

    private void removeCallback(ImageServiceCallBack callback) {
      if (callbacks.containsKey(callback)) {
        callbacks.remove(callback);
      }
    }

    private Bitmap loadImageSynchronous(String url) {
      Bitmap imageFromCache = getImageFromCache(url);

      if (null != imageFromCache) {
        return imageFromCache;
      } else {
        return downloadImageFromNetwork(url);
      }

    }

    private Bitmap downloadImageFromNetwork(String url) {
      if (isCanceled) {
        return null;
      }

      try {
        URL realUrl = new URL(BuildConfig.BASE_API + url);
        // TODO: 08.03.17 set if modified since for better diskcache handling
        InputStream is = realUrl.openConnection().getInputStream();
        Bitmap imageFromNetwork = BitmapFactory.decodeStream(new BufferedInputStream(is));
        if (null != imageFromNetwork) {
          imageCache.put(url, imageFromNetwork);
        }
        return imageFromNetwork;
      } catch (java.io.IOException e) {
        Log.e(TAG, "Cannot download image", e);
      }
      return null;
    }
  }

  @Override
  public void cancelDownload(ImageServiceCallBack clb) {
    if ((null != clb) && (null != clb.getUrl()) && callbacks.containsKey(clb)) {
      Command imageCommand = callbacks.remove(clb);
      backgroundHandler.cancel(imageCommand);
    }
  }

}
