package de.alosdev.heybeach.picture.domain;

import java.util.List;

public class Page {

  private final int pageNumber;
  private final List<Picture> pictures;

  public Page(int pageNumber, List<Picture> pictures) {
    this.pageNumber = pageNumber;
    this.pictures = pictures;
  }

  public int pageNumber() {
    return pageNumber;
  }

  public List<Picture> pictures() {
    return pictures;
  }
}
