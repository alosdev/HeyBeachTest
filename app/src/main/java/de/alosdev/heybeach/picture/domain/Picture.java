package de.alosdev.heybeach.picture.domain;

import android.os.Parcel;
import android.os.Parcelable;

import static android.text.TextUtils.isEmpty;

public class Picture implements Parcelable {

  private final String id;
  private final String name;
  private final String url;
  private final int width;
  private final int height;

  private Picture(String id, String name, String url, int width, int height) {
    this.id = id;
    this.name = name;
    this.url = url;
    this.width = width;
    this.height = height;
  }

  private Picture(Parcel in) {
    id = in.readString();
    name = in.readString();
    url = in.readString();
    width = in.readInt();
    height = in.readInt();
  }

  public String id() {
    return id;
  }

  public String name() {
    return name;
  }

  public String url() {
    return url;
  }

  public int width() {
    return width;
  }

  public int height() {
    return height;
  }

  public static class Builder {
    private String id;
    private String name;
    private String url;
    private int width;
    private int height;

    public Builder setId(String id) {
      this.id = id;
      return this;
    }

    public Builder setName(String name) {
      this.name = name;
      return this;
    }

    public Builder setUrl(String url) {
      this.url = url;
      return this;
    }

    public Builder setWidth(int width) {
      this.width = width;
      return this;
    }

    public Builder setHeight(int height) {
      this.height = height;
      return this;
    }
    public Picture build() {
      if (isEmpty(id) || isEmpty(name) || isEmpty(url)) {
        throw new IllegalArgumentException("fields must be set in advanced");
      } else if (width < 1 || height < 1) {
        throw new IllegalArgumentException("sizes must be larger than 1 pixel");
      }
      return new Picture(id, name, url, width, height);
    }

  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(id);
    dest.writeString(name);
    dest.writeString(url);
    dest.writeInt(width);
    dest.writeInt(height);
  }

  public static final Parcelable.Creator<Picture> CREATOR = new Parcelable.Creator<Picture>() {
    @Override
    public Picture createFromParcel(Parcel source) {
      return new Picture(source);
    }

    @Override
    public Picture[] newArray(int size) {
      return new Picture[size];
    }
  };
}
