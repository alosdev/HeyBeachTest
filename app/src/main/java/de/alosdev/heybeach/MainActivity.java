package de.alosdev.heybeach;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;
import de.alosdev.heybeach.common.base.BaseActivity;
import de.alosdev.heybeach.picture.PictureFragment;
import de.alosdev.heybeach.user.UserFragment;

public class MainActivity extends BaseActivity {

  private static final String BUNDLE_SELECTED_MENU_ITEM_ID = "selectedMenuItemId";

  private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
      = new BottomNavigationView.OnNavigationItemSelectedListener() {

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
      int itemId = item.getItemId();
      //do nothing if it is already selected
      if (itemId == selectedMenutItem) {
        return true;
      }
      selectedMenutItem = itemId;
      if (itemId == R.id.navigation_dashboard) {
        startPictureFragment();
        return true;
      } else if (itemId == R.id.navigation_user) {
        startUserFragment();
        return true;
      }
      return false;
    }
  };

  private int selectedMenutItem = R.id.navigation_dashboard;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_activity);

    BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
    if (null != savedInstanceState) {
      selectedMenutItem = savedInstanceState.getInt(BUNDLE_SELECTED_MENU_ITEM_ID);
    }
    if (null != navigation) {
      if (null == savedInstanceState) {
        startPictureFragment();
      } else {
        if (null == getSupportFragmentManager().findFragmentById(R.id.content)) {
          startPictureFragment();
        }
        navigation.getMenu().findItem(selectedMenutItem).setChecked(true);
      }

      navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    outState.putInt(BUNDLE_SELECTED_MENU_ITEM_ID, selectedMenutItem);
    super.onSaveInstanceState(outState);
  }

  private void startPictureFragment() {
    getSupportFragmentManager().beginTransaction().replace(R.id.content, PictureFragment.newInstance()).commit();
  }

  private int startUserFragment() {
    return getSupportFragmentManager().beginTransaction().replace(R.id.content, UserFragment.newInstance()).commit();
  }
}
