package de.alosdev.heybeach.common.base;

import android.net.http.HttpResponseCache;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

  @Override
  protected void onStop() {
    HttpResponseCache cache = HttpResponseCache.getInstalled();
    if (cache != null) {
      cache.flush();
    }
    super.onStop();
  }
}
