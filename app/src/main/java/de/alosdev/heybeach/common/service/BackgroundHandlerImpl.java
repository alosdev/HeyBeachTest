package de.alosdev.heybeach.common.service;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class BackgroundHandlerImpl implements BackgroundHandler {

  /*
     * Gets the number of available cores
     * (not always the same as the maximum number of cores)
     */
  private static final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
  // Sets the Time Unit to seconds
  private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
  // Sets the amount of time an idle thread waits before terminating
  private static final int KEEP_ALIVE_TIME = 1;

  private final ForegroundHandler foregroundHandler = new ForegroundHandler();
  private final ThreadPoolExecutor decodeThreadPool;

  // Creates a thread pool manager
  public BackgroundHandlerImpl() {
    BlockingQueue<Runnable> mDecodeWorkQueue = new LinkedBlockingDeque<>();
    decodeThreadPool = new ThreadPoolExecutor(
        NUMBER_OF_CORES,       // Initial pool size
        NUMBER_OF_CORES,       // Max pool size
        KEEP_ALIVE_TIME,
        KEEP_ALIVE_TIME_UNIT,
        mDecodeWorkQueue);
  }

  @Override
  public void executeParallel(Command command) {
    decodeThreadPool.execute(new CommandRunnable(foregroundHandler, command));
  }

  @Override
  public void cancel(Command command) {
    command.cancel();
  }

  private static class CommandRunnable implements Runnable {

    private final ForegroundHandler foregroundHandler;
    private final Command cmd;

    CommandRunnable(ForegroundHandler foregroundHandler, Command cmd) {
      this.foregroundHandler = foregroundHandler;
      this.cmd = cmd;
    }

    @Override
    public void run() {
      android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
      cmd.doInBackground();
      foregroundHandler.sendMessage(foregroundHandler.obtainMessage(1, cmd));
    }
  }

  private static class ForegroundHandler extends Handler {

    ForegroundHandler() {
      super(Looper.getMainLooper());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void handleMessage(Message msg) {
      if (msg.obj instanceof Command) {
        ((Command) msg.obj).doInForeground();
      } else {
        throw new UnsupportedOperationException("unknown message type");
      }
    }
  }
}
