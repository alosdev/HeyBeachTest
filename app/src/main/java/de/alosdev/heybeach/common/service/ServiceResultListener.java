package de.alosdev.heybeach.common.service;

public interface ServiceResultListener<Type> {

  /**
   * Notifies that the action has completed successfully.
   *
   * @param result The {@link Type} that was defined for this {@link ServiceResultListener}.
   */
  void success(Type result);

  /**
   * Notifies that an action failed.
   */
  void failed(String message);
}
