package de.alosdev.heybeach.common.service;

public abstract class Command {

  // is not the best way, but need a possibility to cancel during execution
  protected boolean isCanceled;

  void cancel() {
    isCanceled = true;
  }

  public abstract void doInBackground();

  public void doInForeground() {
  }
}
