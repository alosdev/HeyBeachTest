package de.alosdev.heybeach.common.service;

public interface BackgroundHandler {

  void executeParallel(Command command);

  void cancel(Command command);
}
