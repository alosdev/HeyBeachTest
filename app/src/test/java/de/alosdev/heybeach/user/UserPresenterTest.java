package de.alosdev.heybeach.user;

import de.alosdev.heybeach.user.api.UserApiClient;
import de.alosdev.heybeach.user.repo.UserRepo;
import de.alosdev.heybeach.util.EmailMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;

@RunWith(MockitoJUnitRunner.class)
public class UserPresenterTest {

  private UserPresenter presenter;

  @Mock private UserRepo userRepo;
  @Mock private UserApiClient apiClient;
  @Mock private EmailMatcher emailMatcher;
  @Mock private UserPresenter.UserView view;

  @Before
  public void setUp() throws Exception {
    presenter = new UserPresenter(userRepo, apiClient, emailMatcher);
    presenter.bind(view);
    reset(userRepo, apiClient, emailMatcher, view);
  }

  @Test
  public void shouldCallApiLogin() throws Exception {
    given(emailMatcher.matches("email")).willReturn(true);

    presenter.onLogin(true, "email", "password", "");

    then(apiClient).should().login("email", "password");
  }

  @Test
  public void shouldNotCallApiRegisterWithDifferentPasswords() throws Exception {
    given(emailMatcher.matches("email")).willReturn(true);

    presenter.onLogin(false, "email", "password", "");

    then(apiClient).should(never()).register("email", "password");
  }

  @Test
  public void shouldSendValidationErrorWithDifferentPasswords() throws Exception {
    given(emailMatcher.matches("email")).willReturn(true);

    presenter.onLogin(false, "email", "password", "");

    then(view).should().validationErrorPasswordRepeat();
  }

  @Test
  public void shouldCallApiRegisterWithSamePasswords() throws Exception {
    given(emailMatcher.matches("email")).willReturn(true);

    presenter.onLogin(false, "email", "password", "password");

    then(apiClient).should().register("email", "password");
  }

}
